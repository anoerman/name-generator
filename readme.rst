##############
Name Generator
##############

Name Generator adalah aplikasi sederhana yang dibuat atas dasar ke-isengan semata.
Pengguna dapat menyesuaikan nama sesuai dengan beberapa kriteria yang disediakan
seperti jenis kelamin dan jumlah suku kata. Selain itu pengguna juga dapat melakukan
penambahan data nama ke dalam database melakui Master Nama dari bagian pengaturan.

==============
Fitur Aplikasi
==============

1. Generate nama secara acak dari data yang telah disediakan.
2. Melakukan pengaturan data master nama. (CRUD)


================
Kebutuhan Server
================

Di rekomendasikan menggunakan PHP versi 5.6 atau lebih baru, mengikuti kebutuhan
CodeIgniter sebagai framework aplikasinya.

==========
Pemasangan
==========

Langkah untuk melakukan pemasangan aplikasi :

1. Download file aplikasi.
2. Server Produksi / Hosting : Extract file ke dalam folder ``public_html/``.
3. Server Lokal / Localhost  : Extract file ke dalam folder ``htdocs/`` atau ``www/``.
4. Import SQL file ``namegenerator.sql`` yang sudah disediakan dari file aplikasi.
5. Ubah username dan password server pada file ``application/config/database.php`` dan sesuaikan dengan pengaturan server anda.
6. Aplikasi siap digunakan.


=======
Lisensi
=======

Aplikasi ini di set dengan lisensi MIT atau The MIT License.
