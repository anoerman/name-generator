<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** Dashboard Controller  
* Digunakan untuk melakukan generate nama secara random sesuai dengan
*	kriteria yang diberikan oleh pengunjung.
*
* @author   Noerman Agustiyan ( @anoerman )
* @version  1.0
*
*/
class Welcome extends CI_Controller {

	/** Index
	*
	*	@return 	void
	*
	*/
	public function index()
	{
		$datas['title']     = "Generator Nama";
		$datas['result']    = "<h3 class='text-center'>Nama disini ...</h3>";

		// Jika proses post, kembalikan hasil
		if (isset($_POST) && !empty($_POST)) {
			$datas['result'] = $this->calculate(
				$this->input->post('gender'),
				$this->input->post('syllables')
			);
		}

		$this->load->view('partials/_header', $datas);
		$this->load->view('dashboard');
		$this->load->view('partials/_footer');
	}

	/** Proses generate nama
	*
	*	@param 		string 		$gender
	*	@param 		string 		$syllables
	*	@return 	string		$result . $result_2
	*
	*/
	public function calculate($gender, $syllables)
	{
		$exclude  = "";
		$ex       = "";
		$result   = "";
		$result_2 = "";
		// Set gender option
		$gender  = ($gender == "male") ? "male = '1'" : "female = '1'";

		$result_2 .= "<h4>Artinya :</h4><ol>";
		for ($i=0; $i < $syllables; $i++) {
			// Set name position option
			if ($i == 0) { $pos = "front = '1'"; }
			elseif ($i == $syllables-1) { $pos = "last = '1'"; }
			else { $pos = "middle = '1'"; }

			// Set to query
			$query = "SELECT id,name, meaning, origin FROM names WHERE $gender AND $pos $exclude ORDER BY rand() LIMIT 1";

			// Set result and exclude
			$qr = $this->db->query($query);
			foreach ($qr->result() as $data) {
				$result .= $data->name . " ";
				$result_2 .= "<li>" . $data->name . " : " . $data->meaning . " (" . $data->origin . "). </li>";
				$ex .="'$data->id',";
			}

			// Set exclude
			$exclude = "AND id NOT IN (". $ex ."'')";
		}
		$result_2 .= "</ol>";

		return "<h3 class='text-center'>".trim($result)."</h3><br>".trim($result_2);
	}
}
