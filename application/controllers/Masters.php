<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** Master Controller
* Digunakan untuk melakukan pengaturan data master
*
* @author   Noerman Agustiyan ( @anoerman )
* @version  1.0
*
*/
class Masters extends CI_Controller {

  function __construct()
	{
		parent::__construct();

		$this->load->model('Master_model');
	}

  /** Index
  *
  *	@return 	void
  *
  */
	public function index()
	{
		$datas['title'] = "Master Nama";
    $datas['datas'] = $this->Master_model->get_name();

		$this->load->view('partials/_header', $datas);
		$this->load->view('master/index');
		$this->load->view('partials/_footer');
	}

  /** Tambah nama baru
	*
	*  @return   void
	*
	*/
  public function add()
  {
    $datas['title'] = "Generator Nama";
    $datas['info']  = "";
    $datas['type']  = "info";

    // Jika proses post, simpan data
		if (isset($_POST) && !empty($_POST)) {
      // input validation rules
			$this->form_validation->set_rules('name', 'Nama', 'alpha_numeric_spaces|trim|required');
			$this->form_validation->set_rules('meaning', 'Arti Nama', 'alpha_numeric_spaces|trim|required');
			$this->form_validation->set_rules('origin', 'Asal Nama', 'alpha_numeric_spaces|trim|required|callback__name_origin_check['.$this->input->post('name').']');

      // validation run
			if ($this->form_validation->run() === TRUE) {
        $new_name = array(
          'name'    => ucwords($this->input->post('name')),
          'meaning' => ucwords($this->input->post('meaning')),
          'origin'  => ucwords($this->input->post('origin')),
          'male'    => ($this->input->post('gender') == 1) ? 1 : 0,
          'female'  => ($this->input->post('gender') == 0) ? 1 : 0,
          'front'   => (null !== $this->input->post('front')) ? 1 : 0,
          'middle'  => (null !== $this->input->post('middle')) ? 1 : 0,
          'last'    => (null !== $this->input->post('last')) ? 1 : 0,
        );
        $proc = $this->Master_model->insert_name($new_name);
        if ($proc) {
          $datas['info'] = "Nama baru berhasil disimpan";
          $datas['type'] = "success";
        }
        else {
          $datas['info'] = "Nama gagal disimpan. Silahkan coba lagi.";
          $datas['type'] = "danger";
        }
      }
      else {
        // set the flash data error message if there is one
        $datas['info'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        $datas['type'] = "danger";
      }

		}
    // End Jika proses post, simpan data

		$this->load->view('partials/_header', $datas);
		$this->load->view('master/add');
		$this->load->view('partials/_footer');
  }

  /** Ubah data nama
	*
	*  @param    string    $id
	*  @return   void
	*
	*/
  public function edit($id)
  {
    $datas['title']  = "Generator Nama";
    $datas['info']   = "";
    $datas['type']   = "info";
    $datas['id']     = $id;
    $datas['detail'] = $this->Master_model->get_name($id);

    // Jika proses post, simpan data
		if (isset($_POST) && !empty($_POST)) {
      foreach ($datas['detail']->result() as $def_data) {
        $current_name   = $def_data->name;
        $current_origin = $def_data->origin;
      }

      // input validation rules
			$this->form_validation->set_rules('name', 'Nama', 'alpha_numeric_spaces|trim|required');
			$this->form_validation->set_rules('meaning', 'Arti Nama', 'alpha_numeric_spaces|trim|required');
      // Hanya jika nama berbeda dari nama awal - jika nama sama dengan awal, data tetap bisa disimpan tanpa alert error validasi
      if ($this->input->post('name') !== $current_name && $this->input->post('origin') !== $current_origin) {
        $this->form_validation->set_rules('origin', 'Asal Nama', 'alpha_numeric_spaces|trim|required|callback__name_origin_check['.$this->input->post('name').']');
      }

      // validation run
			if ($this->form_validation->run() === TRUE) {
        $new_name = array(
          'name'    => ucwords($this->input->post('name')),
          'meaning' => ucwords($this->input->post('meaning')),
          'origin'  => ucwords($this->input->post('origin')),
          'male'    => ($this->input->post('gender') == 1) ? 1 : 0,
          'female'  => ($this->input->post('gender') == 0) ? 1 : 0,
          'front'   => (null !== $this->input->post('front')) ? 1 : 0,
          'middle'  => (null !== $this->input->post('middle')) ? 1 : 0,
          'last'    => (null !== $this->input->post('last')) ? 1 : 0,
        );
        $proc = $this->Master_model->update_name($new_name, $id);
        if ($proc) {
          $datas['info'] = "Nama berhasil diubah!";
          $datas['type'] = "success";
        }
        else {
          $datas['info'] = "Nama gagal diubah. Silahkan coba lagi.";
          $datas['type'] = "danger";
        }
      }
      else {
        // set the flash data error message if there is one
        $datas['info'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
        $datas['type'] = "danger";
      }

		}
    // End Jika proses post, simpan data

		$this->load->view('partials/_header', $datas);
		$this->load->view('master/edit');
		$this->load->view('partials/_footer');
  }

  /** Callback cek nama dan asal
	*
	*  @param    string    $origin
	*  @param    string    $name
	*  @return   bool
	*
	*/
  public function _name_origin_check($origin, $name)
	{
		$datas = $this->Master_model->name_origin_check(trim($origin), trim($name));
		$total = count($datas->result());
		if ($total == 0) {
			return TRUE;
		}
		else {
			$this->form_validation->set_message('_name_origin_check', 'Nama "'.$name.'" sudah terdaftar di database!');
			return FALSE;
		}
	}

  /** Hapus nama
	*
	*  @param    string    $id
	*  @return   void
	*
	*/
  public function delete($id)
  {
    $proc = $this->Master_model->delete_name($id);
    if ($proc) {
      $datas['info'] = "Nama berhasil dihapus!";
    }
    else {
      $datas['info'] = "Nama gagal dihapus.";
    }
    redirect('masters', 'refresh');
  }
}
