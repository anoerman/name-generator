<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/** Master model 
*
* @author   Noerman Agustiyan ( @anoerman )
* @version  1.0
*
*/
class Master_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();

		$this->table = 'names';
	}

  /** Ambil data nama
	*
	*  @param    string    $id
	*  @return   array
	*
	*/
  public function get_name($id="")
  {
    $this->db->select(
      $this->table.".id ,".
      $this->table.".name ,".
      $this->table.".meaning ,".
      $this->table.".origin ,".
      $this->table.".male ,".
      $this->table.".female, ".
      $this->table.".front, ".
      $this->table.".middle, ".
      $this->table.".last "
    );

    // Set id if available
    if ($id!="") {
      $this->db->where($this->table.'.id', $id);
    }

    // Return
    return $this->db->get($this->table);
  }

  /** Tambah nama baru
	*
	*  @param    array    $datas
	*  @return   bool
	*
	*/
  public function insert_name($datas)
  {
    if ($this->db->insert($this->table, $datas)) {
			return TRUE;
		}
		return FALSE;
  }

  /** Ubah nama
	*
	*  @param    array     $datas
	*  @param    string    $id
	*  @return   bool
	*
	*/
  public function update_name($datas, $id)
  {
    $this->db->where('id', $id);
    if ($this->db->update($this->table, $datas)) {
			return TRUE;
		}
		return FALSE;
  }

  /** Callback cek nama dan asal (origin) untuk menghindari
	*  data duplikat
	*
	*  @param    string    $origin
	*  @param    string    $name
	*  @return   array     $datas
	*
	*/
  public function name_origin_check($origin, $name)
  {
    $this->db->select('id');
    $this->db->where('name', $name);
    $this->db->where('origin', $origin);
    $datas = $this->db->get($this->table);
    return $datas;
  }

  /** Hapus nama
	*
	*  @param    string    $id
	*  @return   bool
	*
	*/
  public function delete_name($id)
  {
    $this->db->where('id', $id);
    if ($this->db->delete($this->table)) {
			return TRUE;
		}
		return FALSE;
  }

}
