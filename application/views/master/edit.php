<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <?php if ($info!="") { echo '<div class="alert alert-'.$type.' alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$info.'</div>'; } ?>
    <form class="form" action="<?php echo base_url('masters/edit/'.$id) ?>" method="post" autocomplete="off">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <i class="fa fa-pencil"></i> &nbsp; Ubah Data Nama
          </h3>
        </div>
        <?php
        foreach ($detail->result() as $dt) {
          $def_name    = $dt->name;
          $def_meaning = $dt->meaning;
          $def_origin  = $dt->origin;
          $def_male    = $dt->male;
          $def_female  = $dt->female;
          $def_front   = $dt->front;
          $def_middle  = $dt->middle;
          $def_last    = $dt->last;
        }
        ?>
        <div class="panel-body">
          <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Nama" value="<?php echo set_value('name', $def_name) ?>" required>
          </div>
          <div class="form-group">
            <label for="meaning">Arti</label>
            <input type="text" class="form-control" name="meaning" id="meaning" placeholder="Arti Nama" value="<?php echo set_value('meaning', $def_meaning) ?>" required>
          </div>
          <div class="form-group">
            <label for="origin">Asal</label>
            <input type="text" class="form-control" name="origin" id="origin" placeholder="Asal Nama" value="<?php echo set_value('origin', $def_origin) ?>" required>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="gender">Jenis Kelamin</label>
                <div class="radio">
                  <label>
                    <input type="radio" name="gender" id="gender" value="1" <?php echo  set_radio('gender', '1', ($def_male == 1) ? TRUE : FALSE); ?>>
                    Laki - laki
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="gender" id="gender" value="0" <?php echo  set_radio('gender', '0', ($def_female == 1) ? TRUE : FALSE); ?>>
                    Perempuan
                  </label>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="orders">Urutan Nama</label>
                <div class="checkbox">
                  <label for="front">
                    <input type="checkbox" name="front" id="front" value="1" <?php echo set_checkbox('front', '1', ($def_front == 1) ? TRUE : FALSE); ?>>
                    Depan
                  </label>
                </div>
                <div class="checkbox">
                  <label for="middle">
                    <input type="checkbox" name="middle" id="middle" value="1" <?php echo set_checkbox('middle', '1', ($def_middle == 1) ? TRUE : FALSE); ?>>
                    Tengah
                  </label>
                </div>
                <div class="checkbox">
                  <label for="last">
                    <input type="checkbox" name="last" id="last" value="1" <?php echo set_checkbox('last', '1', ($def_last == 1) ? TRUE : FALSE); ?>>
                    Belakang
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="panel-footer text-center">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?php echo base_url('masters') ?>" class="btn btn-danger">Kembali</a>
        </div>
      </div>
    </form>
  </div>
</div>
