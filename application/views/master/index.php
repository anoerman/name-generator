<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">
          Master Nama
          <span class="pull-right"><a href="<?php echo base_url('masters/add'); ?>" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> &nbsp; Tambah Nama</a></span>
          <div class="clearfix"></div>
        </h3>
      </div>

      <div class="panel-body">
        <table class="table table-striped table-bordered table-hover datatables">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Arti</th>
              <th>Asal</th>
              <th>JK</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
          <?php $no = 1;
          foreach ($datas->result() as $data): ?>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $data->name ?></td>
              <td><?php echo $data->meaning ?></td>
              <td><?php echo $data->origin ?></td>
              <td><?php if($data->male == 1) { echo "Laki -Laki"; } elseif($data->female == 1) { echo "Perempuan"; } ?></td>
              <td><div class="btn-group btn-group-sm">
                <a href="<?php echo base_url('masters/edit/'.$data->id); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a>
                <a href="<?php echo base_url('masters/delete/'.$data->id); ?>" class="btn btn-danger" onclick="return confirm('Yakin hapus data nama ini?\nData yang sudah dihapus tidak dapat dikembalikan.')"><span class="glyphicon glyphicon-trash"></span></a>
              </div></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>

      <div class="panel-footer text-center">
        <br>
        <p><a href="<?php echo base_url() ?>" class="btn btn-success">Halaman Depan</a></p>
        <p>Dibuat berdasarkan keisengan semata menggunankan CodeIgniter versi <?php echo CI_VERSION ?> oleh <a href="https://gitlab.com/anoerman">@anoerman</a> </p>
      </div>
    </div>
  </div>
</div>
