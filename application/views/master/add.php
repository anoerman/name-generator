<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <?php if ($info!="") { echo '<div class="alert alert-'.$type.' alert-dismissable" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$info.'</div>'; } ?>
    <form class="form" action="<?php echo base_url('masters/add') ?>" method="post" autocomplete="off">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">
            <i class="fa fa-plus"></i> &nbsp; Tambah Nama Baru
          </h3>
        </div>

        <div class="panel-body">
          <div class="form-group">
            <label for="name">Nama</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Nama" value="<?php echo set_value('name') ?>" required>
          </div>
          <div class="form-group">
            <label for="meaning">Arti</label>
            <input type="text" class="form-control" name="meaning" id="meaning" placeholder="Arti Nama" value="<?php echo set_value('meaning') ?>" required>
          </div>
          <div class="form-group">
            <label for="origin">Asal</label>
            <input type="text" class="form-control" name="origin" id="origin" placeholder="Asal Nama" value="<?php echo set_value('origin') ?>" required>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="gender">Jenis Kelamin</label>
                <div class="radio">
                  <label>
                    <input type="radio" name="gender" id="gender" value="1" <?php echo  set_radio('gender', '1', TRUE); ?>>
                    Laki - laki
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="gender" id="gender" value="0" <?php echo  set_radio('gender', '0'); ?>>
                    Perempuan
                  </label>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="orders">Urutan Nama</label>
                <div class="checkbox">
                  <label for="front">
                    <input type="checkbox" name="front" id="front" value="1" <?php echo set_checkbox('front', '1'); ?>>
                    Depan
                  </label>
                </div>
                <div class="checkbox">
                  <label for="middle">
                    <input type="checkbox" name="middle" id="middle" value="1" <?php echo set_checkbox('middle', '1'); ?>>
                    Tengah
                  </label>
                </div>
                <div class="checkbox">
                  <label for="last">
                    <input type="checkbox" name="last" id="last" value="1" <?php echo set_checkbox('last', '1'); ?>>
                    Belakang
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="panel-footer text-center">
          <button type="submit" class="btn btn-success">Simpan</button>
          <a href="<?php echo base_url('masters') ?>" class="btn btn-danger">Kembali</a>
        </div>
      </div>
    </form>
  </div>
</div>
