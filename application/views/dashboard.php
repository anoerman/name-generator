<div class="row">
  <div class="col-md-6">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title text-center">Generator</h3>
      </div>
      <form class="form form-horizontal" action="<?php echo base_url() ?>" method="post">
        <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="gender" class="col-md-4 control-label">Jenis Kelamin :</label>
                  <div class="col-md-8">
                    <div class="radio">
                      <label for="male">
                        <input type="radio" name="gender" id="male" value="male" <?php echo set_radio('gender','male', true) ?> aria-label="Male?" data-labelauty="Laki laki|Nama Laki laki" class="generator">
                      </label>
                    </div>
                    <div class="radio">
                      <label for="female">
                        <input type="radio" name="gender" id="female" value="female" <?php echo set_radio('gender','female') ?> aria-label="Female?" data-labelauty="Perempuan|Nama Perempuan" class="generator">
                      </label>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="form-group">
                  <label for="syllables" class="col-md-4 control-label">Suku Kata :</label>
                  <div class="col-md-8">
                    <div class="radio">
                      <label for="syl1">
                        <input type="radio" name="syllables" id="syl1" value="1" <?php echo set_radio('syllables','1', true) ?> aria-label="Satu" data-labelauty="Satu|Satu Suku Kata" class="generator">
                      </label>
                    </div>
                    <div class="radio">
                      <label for="syl2">
                        <input type="radio" name="syllables" id="syl2" value="2" <?php echo set_radio('syllables','2') ?> aria-label="Dua" data-labelauty="Dua|Dua Suku Kata" class="generator">
                      </label>
                    </div>
                    <div class="radio">
                      <label for="syl3">
                        <input type="radio" name="syllables" id="syl3" value="3" <?php echo set_radio('syllables','3') ?> aria-label="Tiga" data-labelauty="Tiga|Tiga Suku Kata" class="generator">
                      </label>
                    </div>
                    <div class="radio">
                      <label for="syl4">
                        <input type="radio" name="syllables" id="syl4" value="4" <?php echo set_radio('syllables','4') ?> aria-label="Empat" data-labelauty="Empat|Empat Suku Kata" class="generator">
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="panel-footer text-center">
          <button type="submit" name="button" class="btn btn-success">Dapatkan Nama</button>
        </div>
      </form>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title text-center">Hasil</h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-md-12">
            <?php echo $result; ?>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        Page rendered in <strong>{elapsed_time}</strong> seconds.
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title text-center">Pengaturan</h3>
      </div>
      <div class="panel-body">
        <p>Anda dapat melakukan pengaturan tambahan disini :</p>
        <div class="btn-group btn-group-sm">
          <a href="<?php echo base_url('masters') ?>" class="btn btn-success btn-lg">
            <span class="glyphicon glyphicon-cog"></span> Master Nama
          </a>
        </div>
      </div>
      <div class="panel-footer">
        <p>Dibuat berdasarkan keisengan semata. <a href="https://gitlab.com/anoerman">@anoerman</a> </p>
      </div>
    </div>

  </div>
</div>
