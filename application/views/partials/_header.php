<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $title ?></title>
		<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-labelauty/source/jquery-labelauty.css') ?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css') ?>">

	</head>
	<body>
		<div class="container">
			<div class="row">
				<br>
			  <div class="col-lg-12">
